#!/bin/bash
set -e

if [ "$1" = "haproxy" ]; then
	HAPROXYDIR="/usr/local/etc/haproxy"

	if [ -z "$ADMIN_PASSWORD" ]; then
		export ADMIN_PASSWORD="$(cat /dev/urandom | tr -dc A-Z-a-z-0-9 | head -c 18)"
	fi

	# HAproxy chroot directory creation
	mkdir -p /var/lib/haproxy/

	# HAproxy superadmin password configuration
	sed -i "s/STATS_ADMIN_PASSWORD/$ADMIN_PASSWORD/g" "${HAPROXYDIR}/haproxy.cfg"

cat << EOB
****************************************************
*                                                  *
*    Docker image: enzzito/haproxy                 *
*    https://gitlab.com/docker-files1/haproxy      *
*                                                  *
****************************************************
SERVER SETTINGS
---------------
· Default path: $HAPROXYDIR
· Custom path: ${HAPROXYDIR}/conf.d/
· Certificates path: ${HAPROXYDIR}/certs/
· STATS admin user: superadmin
· STATS admin password: $ADMIN_PASSWORD

EOB

  set -- "$@" -W -f "${HAPROXYDIR}/haproxy.cfg" -f "${HAPROXYDIR}/conf.d/01-frontend.cfg" -f "${HAPROXYDIR}/conf.d/02-backend.cfg" -db
fi

exec "$@"
