# 0.8.0
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*
* Se retira soporte para armv7

# 0.7.0
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# 0.6.1
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# 0.6.0
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# 0.5.0
* Se ajusta el `README` para que muestre información relevante
* Se ajusta el `Jenkinsfile` para informar sobre el path donde se encuentra las `Shared libs`
* Se actualiza la configuración por defecto del *.editorconfig* y el *.gitignore*

# 0.4.0
* Inicialización del `CHANGELOG`
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `dockerfilePipeline`
    * Se eliminan del Dockerfile, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.
* Lintado del ficheros `Vagrantfile`
