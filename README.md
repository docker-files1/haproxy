[![pipeline status](https://gitlab.com/docker-files1/haproxy/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/haproxy/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/haproxy?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/haproxy) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/haproxy?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/haproxy)

# HAProxy
Image created over debian buster-slim with haproxy 2.3.1

## Compliance
 - SSL ciphers adjustment
 - By default is enabled the traffic for 443 port
    -   The file used is **alldomains.pem** and is start empty

## Customization
 - Enable stats on port 8181
 - The admin user is 'superadmin' and the password is generated in random mode if not defined
 - Enabled the package **nc** for check container status on the ***stats service port***

## References
https://www.haproxy.com/documentation/hapee/latest/administration/docker-logging/

## Advices
For persistent volume configuration you may think on this paths:
- /usr/local/etc/haproxy/
- /var/lib/haproxy/

Environment variables
----

This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `ADMIN_PASSWORD`
* Default value: Random string.
* Accepted values: Any string.
* Description: If you don't specify a password for the default stats account through `ADMIN_PASSWORD`, a 16 character random string will be automatically generated. You can obtain this value through the [container logs](https://docs.docker.com/engine/reference/commandline/container_logs/).
