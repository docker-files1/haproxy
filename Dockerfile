FROM haproxy:2.3.4

LABEL DistBase="Debian 10 - Buster"

RUN apt update -y && \
    apt upgrade -y && \
    apt install -y netcat --no-install-recommends && \
    apt autoremove -y && apt clean -y && \
    rm -rf /var/lib/apt/lists/*

# Debug mode
# RUN apt install -y procps iputils-ping net-tools telnet vim

# HAproxy configuration
COPY configs/ /usr/local/etc/haproxy/

# HAproxy pre-configurations
COPY scripts/docker-entrypoint.sh /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

EXPOSE 80/tcp
EXPOSE 443/tcp
EXPOSE 8181/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD nc -z localhost 8181 || exit 1

CMD ["haproxy"]
#docker build -t oscarenzo/haproxy:latest .
#docker buildx build --push -t oscarenzo/haproxy:latest --platform linux/amd64,linux/arm/v7 .
